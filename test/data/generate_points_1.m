clc
clear
rng(23497);

N_size = 3000;

pc = 1.5 * rand(3, N_size);
pc_homo = [pc; ones(1, size(pc, 2))];
angles = [27.5 42.7 62.37];
translation = [0.18, 0.22, -0.14];

tform = rigidtform3d(angles,translation);
pc_transformed = tform.A * pc_homo;

pc_center = mean(pc, 2);
pc_transformed_center = mean(pc_transformed(1:3, :), 2);

H = (pc - pc_center) * (pc_transformed(1:3, :) - pc_transformed_center)';
[U, S, V] = svd(H);
R = V * U';
t = pc_transformed_center - R * pc_center;

noise = 0.5 * randn(3, N_size);
noise_id = rand(3, N_size) < 0.5;
outlier_noise = randn(3, N_size) < 0.05;
pc_out = pc_transformed(1:3, N_size) + noise .* noise_id + 2 * outlier_noise;
pc_out_center = mean(pc_out, 2);
H_out = (pc - pc_center) * (pc_out - pc_out_center)';
[U_out, S, V_out] = svd(H_out);
R_out = V_out * U_out';
t_out = pc_out_center - R_out * pc_center;

writematrix(pc', "in_points_1.txt");
writematrix(pc_transformed(1:3, :)', "out_points_1.txt");
writematrix(pc_out', "noised_points_1.txt");