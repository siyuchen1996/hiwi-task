clc
clear
rng(23497);

N_size = 9000;

pc = 4 * rand(3, N_size);
pc_homo = [pc; ones(1, size(pc, 2))];
angles = [42.89 12.37 52.75];
translation = [0.5863, 0.2342, -0.6314];

tform = rigidtform3d(angles,translation);
pc_transformed = tform.A * pc_homo;

pc_center = mean(pc, 2);
pc_transformed_center = mean(pc_transformed(1:3, :), 2);

H = (pc - pc_center) * (pc_transformed(1:3, :) - pc_transformed_center)';
[U, S, V] = svd(H);
R = V * U';
t = pc_transformed_center - R * pc_center;

noise = 0.5 * randn(3, N_size);
outlier_noise = rand(3, N_size) < 0.2;
pc_out = pc_transformed(1:3, N_size) + noise + outlier_noise;
pc_out_center = mean(pc_out, 2);
H_out = (pc - pc_center) * (pc_out - pc_out_center)';
[U_out, S, V_out] = svd(H_out);
R_out = V_out * U_out';
t_out = pc_out_center - R_out * pc_center;

writematrix(pc', "in_points_2.txt");
writematrix(pc_transformed(1:3, :)', "out_points_2.txt");
writematrix(pc_out', "noised_points_2.txt");