/**
BSD 3-Clause License

Copyright (c) 2024, Siyu Chen siyu.chen@gia.rwth-aachen.de .
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "Hiwi/numerical_solution.h"
#include "Hiwi/utility.h"
#include <gtest/gtest.h>

const std::string projectPath = PROJECT_SOURCE_DIR;

void buildProblem(ceres::Problem& problem,
                  const task::PointsD& inPoints,
                  const task::PointsD& outPoints,
                  Eigen::Vector3d& translationEstimate,
                  Eigen::Quaternion<double>& quaternionEstimate,
                  ceres::LossFunction* loss = nullptr){
    ceres::Manifold* quaternion_manifold = new task::MyQuaternionManifold;
    const int nb = inPoints.rows();
    for (int i = 0; i < nb; ++i) {
        problem.AddResidualBlock(
                new ceres::AutoDiffCostFunction<task::PoseErrorResidual, 3, 4, 3>(
                        new task::PoseErrorResidual(inPoints.row(i),
                                              outPoints.row(i))),
                loss,
                quaternionEstimate.coeffs().data(),
                translationEstimate.data());
        problem.SetManifold(quaternionEstimate.coeffs().data(),
                            quaternion_manifold);
    }
}

// test numerical method on a smaller dataset without any noise
TEST(NumericalImpl, CeresSmallTest){
    // read point sets
    task::PointsD inPoints = task::loadData(projectPath + "/test/data/in_points_1.txt");
    task::PointsD outPoints = task::loadData(projectPath + "/test/data/out_points_1.txt");

    // ground truth translation and rotation
    Eigen::MatrixX3d rotation(3, 3);
    rotation << 0.340823982872424, -0.640635423624230, 0.688059057564669,
            0.651105578891325,	0.688789289483572,	0.318795921914490,
            -0.678159669868071,	0.339345795156680,	0.651877207358801;
    Eigen::Vector3d translation(3);
    translation << 0.18, 0.22, -0.14;

    // initial estimate of quaternion and translation
    Eigen::Quaternion<double> quaternionEstimate(1, 0, 0, 0);
    Eigen::Vector3d translationEstimate(0, 0, 0);

    // build problem and solve it
    ceres::Problem problem;
    buildProblem(problem, inPoints, outPoints, translationEstimate, quaternionEstimate);
    ceres::Solver::Options options;
    options.max_num_iterations = 20;
    options.linear_solver_type = ceres::DENSE_QR;
//    options.minimizer_progress_to_stdout = true;

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);

    EXPECT_TRUE(quaternionEstimate.toRotationMatrix().isApprox(rotation, 1e-5));
    EXPECT_TRUE(translationEstimate.isApprox(translation, 1e-5));
}


// test numerical method on a larger dataset without any noise
TEST(NumericalImpl, CeresLargeTest){
    task::PointsD inPoints = task::loadData(projectPath + "/test/data/in_points_2.txt");
    task::PointsD outPoints = task::loadData(projectPath + "/test/data/out_points_2.txt");

    Eigen::MatrixX3d rotation(3, 3);
    rotation << 0.591241833646878, -0.494948739078016, 0.636756499637782,
            0.777522481409158, 0.559532252205230, -0.287023430481708,
            -0.214223913345879, 0.664792752989400, 0.715652646556660;
    Eigen::Vector3d translation(3);
    translation << 0.5863, 0.2342, -0.6314;

    Eigen::Quaternion<double> quaternionEstimate(1, 0, 0, 0);
    Eigen::Vector3d translationEstimate(0, 0, 0);

    ceres::Problem problem;
    buildProblem(problem, inPoints, outPoints, translationEstimate, quaternionEstimate);
    ceres::Solver::Options options;
    options.max_num_iterations = 20;
    options.linear_solver_type = ceres::DENSE_QR;
//    options.minimizer_progress_to_stdout = true;

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);

    EXPECT_TRUE(quaternionEstimate.matrix().isApprox(rotation, 1e-5));
    EXPECT_TRUE(translationEstimate.isApprox(translation, 1e-5));
}

// test numerical method on a smaller dataset with white noise and outliers
TEST(NumericalImpl, CeresSmallNoiseTest){
    task::PointsD inPoints = task::loadData(projectPath + "/test/data/in_points_1.txt");
    task::PointsD outPoints = task::loadData(projectPath + "/test/data/noised_points_1.txt");

    Eigen::MatrixX3d rotation(3, 3);
    rotation << 0.340823982872424, -0.640635423624230, 0.688059057564669,
            0.651105578891325,	0.688789289483572,	0.318795921914490,
            -0.678159669868071,	0.339345795156680,	0.651877207358801;
    Eigen::Vector3d translation(3);
    translation << 0.18, 0.22, -0.14;

    Eigen::Quaternion<double> quaternionEstimate(1, 0, 0, 0);
    Eigen::Vector3d translationEstimate(0, 0, 0);

    ceres::Problem problem;
    buildProblem(problem,
                 inPoints,
                 outPoints,
                 translationEstimate,
                 quaternionEstimate,
                 new ceres::HuberLoss(0.5));
    ceres::Solver::Options options;
    options.max_num_iterations = 20;
    options.linear_solver_type = ceres::DENSE_QR;
//    options.minimizer_progress_to_stdout = true;

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);

    double errorEstimate;
    task::poseError(inPoints, outPoints, translationEstimate, quaternionEstimate, errorEstimate);

    double error;
    task::poseError(inPoints, outPoints, translation, rotation, error);

    EXPECT_LE(errorEstimate, error);
}

// test numerical method on a larger dataset with white noise and outliers
TEST(NumericalImpl, CeresLargeNoiseTest){
    task::PointsD inPoints = task::loadData(projectPath + "/test/data/in_points_2.txt");
    task::PointsD outPoints = task::loadData(projectPath + "/test/data/noised_points_2.txt");

    Eigen::MatrixX3d rotation(3, 3);
    rotation << 0.591241833646878, -0.494948739078016, 0.636756499637782,
            0.777522481409158, 0.559532252205230, -0.287023430481708,
            -0.214223913345879, 0.664792752989400, 0.715652646556660;
    Eigen::Vector3d translation(3);
    translation << 0.5863, 0.2342, -0.6314;

    Eigen::Quaternion<double> quaternionEstimate(1, 0, 0, 0);
    Eigen::Vector3d translationEstimate(0, 0, 0);

    ceres::Problem problem;

    buildProblem(problem,
                 inPoints,
                 outPoints,
                 translationEstimate,
                 quaternionEstimate,
                 new ceres::HuberLoss(0.5));

    ceres::Solver::Options options;
    options.max_num_iterations = 5;
    options.linear_solver_type = ceres::DENSE_QR;
//    options.minimizer_progress_to_stdout = true;

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);

    double errorEstimate;
    task::poseError(inPoints, outPoints, translationEstimate, quaternionEstimate, errorEstimate);

    double error;
    task::poseError(inPoints, outPoints, translation, rotation, error);

    EXPECT_LE(errorEstimate, error);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}