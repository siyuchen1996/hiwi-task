# Tasks Instruction

## Getting Started

### Cloning Project
Use `git` to clone the project from `gitlab rwth aachen` using the following command. You should have configured
`git` on your own computer.
```shell
# replace {absolute-path-of-your-favorite-folder} with a path where you want to clone the project
cd {absolute-path-of-your-favorite-folder}
# if you use https
git clone https://git.rwth-aachen.de/siyuchen1996/hiwi-task.git
# or you use ssh
git clone git@git.rwth-aachen.de:siyuchen1996/hiwi-task.git
```

### Installing Docker
For this case study, we have prepared a docker image named `hiwi-task` for you to test your implementation. In order to 
use it, please install [Docker](https://docs.docker.com/engine/install/) at first and then make yourself familiar with it.

### Pulling Docker Image
Once you have installed the `Docker`, run the following command to pull the pre-built docker image to set up the environment.
```shell
docker pull registry.git.rwth-aachen.de/siyuchen1996/hiwi-task:latest 
```

### Interacting with Docker Image
Open the terminal and execute the following command to run the docker container based on the docker image.
```shell
docker run -it -v {absolute-path-of-your-folder}/hiwi-task:/task \
  --name hiwi-task-env registry.git.rwth-aachen.de/siyuchen1996/hiwi-task:latest
```
If your operation is correct, now you can interact with the docker container. Using the following command to verify it.
```shell
ls
# if your operation is correct, you should see something like:
# CMakeLists.txt  Dockerfile  LICENSE  README.md  include  res src  test
```

## Building the Project
Within the docker container, please use `ninja` to build the project.
```shell
mkdir build && cd build
cmake -G Ninja ..
```

## Testing Your Implementation
### Task: `undistortPoints`
Run the following commands to build the test target and execute the tests.
```shell
# make sure you are in the build folder
ninja test_undistortion
cd test && ./test_undistortion
```
If your implementation is correct, you could see following outputs.


![Test Undistortion](res/test_undistortion_success.png)

### Task: `directSVD`
Run the following commands to build the test target and execute the tests.
```shell
# make sure you are in the build folder
ninja test_directsvd
cd test && ./test_directsvd
```
If your implementation is correct, you could see following outputs.


![Test DirectSVD](res/test_directsvd_success.png)

### Task: Numerical Solution
Run the following commands to build the test target and execute the tests.
```shell
# make sure you are in the build folder
ninja test_numerical_solution
cd test && ./test_numerical_solution
```
If your implementation is correct, you could see following outputs.


![Test Numerical Solution](res/test_numerical_solution_success.png)

## License
The code is designed for the case study **Optimization and Numerical Mathematics for Computer Vision** and is provided under a BSD 3-clause license. See the LICENSE file for details.

Parts of the code (`include/Hiwi/numerical_solution.h`) are adapted from [ceres library](http://ceres-solver.org/) and distributed under an Apache-2.0 licence.

