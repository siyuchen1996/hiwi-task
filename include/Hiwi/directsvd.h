/**
BSD 3-Clause License

Copyright (c) 2024, Siyu Chen siyu.chen@gia.rwth-aachen.de .
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef HIWI_DIRECTSVD_H
#define HIWI_DIRECTSVD_H

#include <Eigen/Core>
#include <Eigen/SVD>
#include <glog/logging.h>

namespace task{
    typedef Eigen::MatrixX3d PointsD;

    /**
     * @brief Compute 3d rigid transform using direct SVD decomposition
     *
     * out_points = R * in_points' + t, [R, t] is the so called 3d rigid transformation
     *
     * @param in_points 3d points whose dim = (N, 3) where N is the number of points
     * @param out_points transformed 3d points whose dim = (N, 3) where N is the number of points
     * @param rotation rotation matrix R
     * @param translation translation vector t
     */
    void directSVD(const PointsD& in_points,
                   const PointsD& out_points,
                   Eigen::Matrix3d& rotation,
                   Eigen::Vector3d& translation);
}

#endif //HIWI_DIRECTSVD_H
