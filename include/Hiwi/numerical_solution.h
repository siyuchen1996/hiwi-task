/**
BSD 3-Clause License

Copyright (c) 2024, Siyu Chen siyu.chen@gia.rwth-aachen.de .
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef HIWI_NUMERICAL_SOLUTION_H
#define HIWI_NUMERICAL_SOLUTION_H

#include <Eigen/Core>
#include <ceres/ceres.h>

namespace task{
    typedef Eigen::Vector3d PointD;

    /**
     * @brief A class to calculate the residual error of a pose (rotation and translation)
     *        applied to a 3D point against a target 3D point.
     *
     * This class is designed for use ceres library in optimization problems. The transformation is defined
     * by a quaternion representing rotation and a vector representing translation.
     */
    class PoseErrorResidual {
    public:
        /**
         * @brief Constructs a PoseErrorResidual object with an original and a target point.
         * @param p The original 3D point before transformation.
         * @param q The target 3D point to match after applying the pose transformation.
         *
         * Both points are passed by value and then moved into member variables to optimize
         * memory usage and performance.
         */
        PoseErrorResidual(PointD p, PointD q) :
                p_(std::move(p)),
                q_(std::move(q)) {}

        /**
         * @brief Operator overload to compute the residual error of a pose applied to the original point.
         *
         * This method is templated to allow for compatibility with automatic differentiation
         * and various numeric representations.
         *
         * @tparam T The numeric type for computation (e.g., double, auto-diff types).
         * @param quaternion_ptr Pointer to the first element of an array representing the rotation quaternion.
         * @param trans_ptr Pointer to the first element of an array representing the translation vector.
         * @param residual_ptr Pointer to the storage location for the computed residual.
         * @return Always returns true, indicating that the residual was computed successfully.
         */
        template <typename T>
        bool operator()(const T* const quaternion_ptr, const T* const trans_ptr, T* residual_ptr) const {
            Eigen::Map<const Eigen::Matrix<T, 3, 1>> trans(trans_ptr);
            Eigen::Map<const Eigen::Quaternion<T>> rot(quaternion_ptr);

            Eigen::Map<Eigen::Matrix<T, 3, 1>> residual(residual_ptr);
            residual.template block<3, 1>(0, 0) = q_.template cast<T>() - (rot * p_.template cast<T>() + trans);

            return true;
        }

    private:
        const PointD p_, q_;
    };

    /**
     * @brief Manifold representation for quaternion-based optimization in Ceres Solver.
     *
     * This class implements a manifold for quaternions, providing the necessary
     * operations to work with quaternions within the optimization framework of
     * Ceres Solver. It represents the space of quaternions with a 3-dimensional
     * tangent space, suitable for optimization tasks involving 3D rotations.
     */
    class MyQuaternionManifold final : public ceres::Manifold {
    public:
        /**
         * @brief Returns the size of the ambient space.
         *
         * For a quaternion, which is represented as a 4-dimensional vector, this
         * method returns 4.
         *
         * @return The ambient space dimension, which is 4 for quaternions.
         */
        int AmbientSize() const override { return 4; }

        /**
         * @brief Returns the size of the tangent space.
         *
         * The tangent space for a quaternion manifold is 3-dimensional, corresponding
         * to the minimal representation of 3D rotations.
         *
         * @return The tangent space dimension, which is 3.
         */
        int TangentSize() const override { return 3; }

        /**
         * @brief Applies a perturbation to a point on the manifold.
         *
         * This method updates a quaternion by applying a small rotation represented
         * by the tangent vector delta.
         *
         * @param x The original quaternion.
         * @param delta The tangent vector representing a small rotation.
         * @param x_plus_delta The updated quaternion after applying the rotation.
         * @return True if the operation is successful.
         */
        bool Plus(const double* x,
                  const double* delta,
                  double* x_plus_delta) const override;

        /**
         * @brief Computes the Jacobian of the Plus operation with respect to the tangent vector.
         *
         * @param x The original quaternion.
         * @param jacobian The Jacobian matrix of the Plus operation.
         * @return True if the operation is successful.
         */
        bool PlusJacobian(const double* x, double* jacobian) const override;

        /**
         * @brief Computes the difference between two points on the manifold.
         *
         * This method computes the tangent vector that represents the rotation
         * from quaternion x to quaternion y.
         *
         * @param y The target quaternion.
         * @param x The original quaternion.
         * @param y_minus_x The tangent vector representing the rotation from x to y.
         * @return True if the operation is successful.
         */
        bool Minus(const double* y,
                   const double* x,
                   double* y_minus_x) const override;

        /**
         * @brief Computes the Jacobian of the Minus operation with respect to the point on the manifold.
         *
         * @param x The point on the manifold.
         * @param jacobian The Jacobian matrix of the Minus operation.
         * @return True if the operation is successful.
         */
        bool MinusJacobian(const double* x, double* jacobian) const override;
    };
}

#endif //HIWI_NUMERICAL_SOLUTION_H
