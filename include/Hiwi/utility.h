/**
BSD 3-Clause License

Copyright (c) 2024, Siyu Chen siyu.chen@gia.rwth-aachen.de .
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef HIWI_UTILITY_H
#define HIWI_UTILITY_H

#include <glog/logging.h>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <fstream>
#include <iostream>

namespace task{
    typedef Eigen::MatrixX3d PointsD;
    typedef Eigen::Vector3d PointD;

    /**
     * @brief Load the 3d points into the Eigen Matrix
     *
     * @param dataPath path which stores 3d points
     * @return data matrix whose first dimension is the number of points and second dimension is always 3
     */
    PointsD loadData(const std::string& dataPath);


    /**
     * @brief Computes the average error of the transformed input points relative to the output points.
     *
     * This function calculates the pose error between two sets of 3D points. The transformation
     * applied to the input points consists of a rotation (represented by a quaternion) and a translation.
     * The error is computed as the average Euclidean distance between the transformed input points and
     * the output points.
     *
     * @param inPoints The input points to be transformed.
     * @param outPoints The target points to compare against after transformation.
     * @param translation The translation vector to be applied to the input points.
     * @param quat The quaternion representing the rotation to be applied to the input points.
     * @param error Reference to a double where the computed error will be stored.
     */
    void poseError(const PointsD& inPoints,
                   const PointsD& outPoints,
                   const Eigen::Vector3d& translation,
                   const Eigen::Quaternion<double>& quat,
                   double& error);

    /**
     * Overloaded function to calculate the pose error between two sets of points using a rotation matrix.
     *
     * This version is an overload of the poseError function that uses a quaternion for rotation.
     * It allows for the rotation to be specified directly as a 3x3 rotation matrix.
     *
     * @param inPoints The input points to be transformed.
     * @param outPoints The target points to compare against after transformation.
     * @param translation The translation vector to be applied.
     * @param rotation The 3x3 rotation matrix representing the rotation.
     * @param error The calculated pose error (output parameter).
     */
    void poseError(const PointsD& inPoints,
                   const PointsD& outPoints,
                   const Eigen::Vector3d& translation,
                   const Eigen::Matrix<double, 3, 3>& rotation,
                   double& error);
}
#endif //HIWI_UTILITY_H
