/**
BSD 3-Clause License

Copyright (c) 2024, Siyu Chen siyu.chen@gia.rwth-aachen.de .
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef HIWI_UNDISTORTION_HPP
#define HIWI_UNDISTORTION_HPP

#include <Eigen/Core>
#include <glog/logging.h>
#include <iostream>
#include <type_traits> // For std::is_same

namespace task{
    /**
     * @brief Corrects for lens distortion in 2D points.
     *
     * This function applies a lens distortion correction to a set of 2D points using the specified camera intrinsic
     * matrix and distortion coefficients. It iteratively refines the undistorted points to minimize re-projection error.
     *
     * @tparam Scalar The data type for calculations, must be float or double.
     * @param src The source matrix containing the distorted 2D points, one point per row.
     * @param des The destination matrix where undistorted 2D points will be stored.
     * @param cameraMatrix The camera intrinsic matrix containing the focal lengths and principal point.
     *        Expected format: [fx, fy, cx, cy]^T.
     * @param distCoeffs The distortion coefficients matrix. Supports up to 8 coefficients
     *        (k1, k2, k3, p1, p2, k4, k5, k6)^T.
     * @param maxIteration The maximum number of iterations for the undistortion process. Default is 5.
     * @param maxReprojectionError The maximum allowed reprojection error to stop the iterations. Default is 1e-4.
     *
     * @note The function performs a compile-time check to ensure the Scalar type is either float or double.
     */
    template<typename Scalar>
    void undistortPoints(const Eigen::Matrix<Scalar, Eigen::Dynamic, 2>& src,
                         Eigen::Matrix<Scalar, Eigen::Dynamic, 2>& des,
                         const Eigen::Matrix<Scalar, 4, 1>& cameraMatrix,
                         const Eigen::Matrix<Scalar, 8, 1>& distCoeffs,
                         const int& maxIteration=5,
                         const Scalar& maxReprojectionError=Scalar(1e-4)){
        // Compile-time check to ensure Scalar is either float or double
        static_assert(std::is_same<Scalar, float>::value || std::is_same<Scalar, double>::value,
                      "Scalar must be either float or double.");

        CHECK(src.cols() == 2) << "The columns of the src should 2";
        if(des.cols() == 0 || des.rows() == 0){
            des.resize(src.rows(), 2);
        } else{
            CHECK(src.rows() == des.rows()) << "src and des should have same rows";
            CHECK(des.cols() == 2) <<"The columns of the des should 2";
        }
        const int N = src.rows();
        const Scalar& fx = cameraMatrix(0, 0);
        const Scalar& fy = cameraMatrix(1, 0);
        const Scalar& cx = cameraMatrix(2, 0);
        const Scalar& cy = cameraMatrix(3, 0);
        const Scalar ifx = 1 / fx;
        const Scalar ify = 1 / fy;

        const Scalar& k1 = distCoeffs(0, 0);
        const Scalar& k2 = distCoeffs(1, 0);
        const Scalar& k3 = distCoeffs(2, 0);
        const Scalar& p1 = distCoeffs(3, 0);
        const Scalar& p2 = distCoeffs(4, 0);
        const Scalar& k4 = distCoeffs(5, 0);
        const Scalar& k5 = distCoeffs(6, 0);
        const Scalar& k6 = distCoeffs(7, 0);

        // TODO iterative undistortion algorithm to be implemented
    }
}

#endif //HIWI_UNDISTORTION_HPP
