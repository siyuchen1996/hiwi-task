FROM ubuntu:20.04
LABEL authors="Siyu Chen"
LABEL email="siyu.chen@gia.rwth-aachen.de"

# https://github.com/docker/docs/issues/13980#issuecomment-997740982
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y git \
    cmake \
    libeigen3-dev \
    ninja-build \
    build-essential \
    libgoogle-glog-dev libgflags-dev \
    libatlas-base-dev && apt clean

WORKDIR /thirdparty

RUN git clone https://ceres-solver.googlesource.com/ceres-solver

RUN cd ceres-solver && cmake ../ceres-solver \
    && make -j8 && make install

RUN git clone https://github.com/google/googletest.git -b v1.14.0

RUN cd googletest && mkdir build  \
    && cd build && cmake .. -DBUILD_GMOCK=OFF \
    && make -j8 && make install

WORKDIR /task

CMD ["bash"]