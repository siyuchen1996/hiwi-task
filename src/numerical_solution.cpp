/**
BSD 3-Clause License

Copyright (c) 2024, Siyu Chen siyu.chen@gia.rwth-aachen.de .
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "Hiwi/numerical_solution.h"

bool task::MyQuaternionManifold::Plus(
        const double *x,
        const double *delta,
        double *x_plus_delta) const {

    const double norm_delta = std::hypot(delta[0], delta[1], delta[2]);

    if (std::fpclassify(norm_delta) == FP_ZERO) {
        // No change in rotation: return the quaternion as is.
        std::copy_n(x, 4, x_plus_delta);
        return true;
    }

    // TODO quaternion plus to be implemented

    return true;
}

bool task::MyQuaternionManifold::PlusJacobian(
        const double *x,
        double *jacobian) const {
    Eigen::Map<Eigen::Matrix<double, 4, 3, Eigen::RowMajor>> jacobian_rt(
            jacobian);

    // TODO quaternion plus jacobian to be implemented
    jacobian_rt(3, 0) =
    jacobian_rt(3, 1) =
    jacobian_rt(3, 2) =
    jacobian_rt(0, 0) =
    jacobian_rt(0, 1) =
    jacobian_rt(0, 2) =
    jacobian_rt(1, 0) =
    jacobian_rt(1, 1) =
    jacobian_rt(1, 2) =
    jacobian_rt(2, 0) =
    jacobian_rt(2, 1) =
    jacobian_rt(2, 2) =

    return true;
}

bool task::MyQuaternionManifold::Minus(
        const double *y,
        const double *x,
        double *y_minus_x) const {
    // TODO quaternion minus to be implemented

    return true;
}

bool task::MyQuaternionManifold::MinusJacobian(
        const double *x,
        double *jacobian) const {
    Eigen::Map<Eigen::Matrix<double, 3, 4, Eigen::RowMajor>> jacobian_rt(
            jacobian);

    // TODO quaternion manifold minus jacobian to be implemented
    jacobian_rt(0, 3) =
    jacobian_rt(0, 0) =
    jacobian_rt(0, 1) =
    jacobian_rt(0, 1) =
    jacobian_rt(1, 3) =
    jacobian_rt(1, 0) =
    jacobian_rt(1, 1) =
    jacobian_rt(1, 1) =
    jacobian_rt(2, 3) =
    jacobian_rt(2, 0) =
    jacobian_rt(2, 1) =
    jacobian_rt(2, 1) =

    return true;
}