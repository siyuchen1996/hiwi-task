/**
BSD 3-Clause License

Copyright (c) 2024, Siyu Chen siyu.chen@gia.rwth-aachen.de .
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "Hiwi/utility.h"

namespace task{
    PointsD loadData(const std::string& dataPath){
        std::ifstream file(dataPath);
        if (!file.is_open()) {
            LOG(ERROR) << "Unable to open data file";
            return PointsD(); // Return an empty matrix
        }

        std::string line;
        std::vector<PointD> points;
        points.reserve(1000); // Reserve an estimated size to reduce reallocations

        while (std::getline(file, line)) {
            std::stringstream ss(line);
            Eigen::RowVectorXd point(3);
            std::string value;
            int i = 0;

            while (std::getline(ss, value, ',')) {
                point[i++] = std::stod(value);
            }

            points.push_back(point);
        }
        file.close();

        const int nb = points.size();
        PointsD matrix(nb, 3);
        for (size_t i = 0; i < nb; ++i) {
            matrix.row(i) = points[i];
        }

        return matrix;
    }

    void poseError(const PointsD& inPoints,
                   const PointsD& outPoints,
                   const Eigen::Vector3d& translation,
                   const Eigen::Quaternion<double>& quat,
                   double& error){
        poseError(inPoints, outPoints, translation, quat.toRotationMatrix(), error);
}

    void poseError(const PointsD& inPoints,
                   const PointsD& outPoints,
                   const Eigen::Vector3d& translation,
                   const Eigen::Matrix<double, 3, 3>& rotation,
                   double& error){
    PointsD dis = outPoints - inPoints * rotation.transpose();
    dis = dis.rowwise() - translation.transpose();
    error = dis.rowwise().norm().sum() / inPoints.rows();
    }
}
